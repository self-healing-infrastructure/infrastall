#!/bin/bash
curl -L http://shci-api:8000/infrastructure/install/ansible_host_file/ > hosts.yml
curl -L http://shci-api:8000/infrastructure/install/ansible_var_file/ > group_vars/all.yml
start_cmd="ansible-playbook -i hosts.yml"

export PYTHONWARNINGS=ignore::UserWarning

if [ "$MODE" = "PROVISIONING" ]
then
    $start_cmd provisioning.yml
elif [ "$MODE" = "DEPLOY" ]
then
    echo "Deploy"
fi;
