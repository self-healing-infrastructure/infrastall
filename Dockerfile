FROM debian:10
# Set Ansible repo

# Install dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       sudo systemd sshpass wget gnupg2 curl

RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main" | tee -a /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367


# Install Ansible via pip.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ansible \
       build-essential libffi-dev libssl-dev \
       python-pip python-dev python-setuptools python-wheel \
    && rm -rf /var/lib/apt/lists/* \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
    && apt-get clean
RUN pip install cryptography

RUN ansible-galaxy collection install ansible.posix

ENV ANSIBLE_HOST_KEY_CHECKING=False

WORKDIR /root/playbook/

COPY . .

RUN chmod +x ./infrastall.sh
ENTRYPOINT [ "./infrastall.sh" ]
