# Installation

Installation de docker et docker-compose

# Role Variables

- docker_data_location: Emplacement du stockage des données de docker (image, container, volume...)
- docker_edition: Edition de docker (ce ou ee)
- docker_package: Nom du package docker (docker-ce) (composition de plusieurs variables)
- docker_install_compose: installation de docker-compose (bool)
- docker_compose_version: version de docker-compose
- docker_compose_path: chemin d'installation de docker-compose (/usr/local/bin/docker-compose)
- docker_apt_release_channel: type de release (stable)
- docker_apt_arch: architecture cible (amd64)
- docker_apt_repository: repository docker (composition de plusieurs variables)
- docker_apt_ignore_key_error: Ignore les erreurs la clé apt docker (bool)
- docker_users: Utilisateur qui va utiliser docker
